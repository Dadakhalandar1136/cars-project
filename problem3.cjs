function problem3(inventory) {
    let arryaToStoreModel = [];
    
    if (Array.isArray(inventory) === false || inventory === undefined) {
        return [];
    }
    if (inventory.length === 0) {
        return [];
    } 
    for (let index = 0; index < inventory.length; index++) {
        arryaToStoreModel.push(inventory[index].car_model);
    };
    let toStoreUpper = [];
    for (let index = 0; index < arryaToStoreModel.length; index++) {
        let modelName = arryaToStoreModel[index];
        toStoreUpper.push(modelName.toUpperCase());
    };
    return toStoreUpper.sort();
};

module.exports = problem3;