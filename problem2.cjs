function problem2(inventory) {

    if (Array.isArray(inventory) === false || inventory === undefined) {
        return [];
    }
    if (inventory.length === 0) {
        return [];
    } 
    return inventory[inventory.length-1];
}

module.exports = problem2;