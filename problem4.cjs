function problem4(inventory) {
    let listedYears = [];

    if (Array.isArray(inventory) === false || inventory === undefined) {
        return [];
    }
    if (inventory.length === 0) {
        return [];
    } 
    for (let index = 0; index < inventory.length; index++) {
        listedYears.push(inventory[index].car_year);
    };
    return listedYears;
};

module.exports = problem4;