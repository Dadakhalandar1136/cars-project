let inventory = require('../data.cjs');
let problem1 = require('../problem1.cjs');


const result = problem1(inventory, []);
console.log(result);
if(result.length != 0) {
    console.log("car " + result.id + " is a " + result.car_year + " " + result.car_make + " " + result.car_model);
}
