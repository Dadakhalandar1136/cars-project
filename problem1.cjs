function problem1(inventory, carId) {

    if(inventory === undefined || Array.isArray(inventory) === false) {
        return [];
    }
    if(inventory.length === 0) {
        return [];
    }
    for(let index = 0; index < inventory.length; index++) {
        if(inventory[index].id === carId) {
            return inventory[index];                
        }
        if(index === inventory.length-1) {
            return [];
        }
    }
}

module.exports = problem1;
