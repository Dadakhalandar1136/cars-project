function problem5(inventory) {
    let olderMade = [];
    
    if (Array.isArray(inventory) === false || inventory === undefined) {
        return [];
    }
    if (inventory.length === 0) {
        return [];
    } 
    for (let index = 0; index < inventory.length; index++) {
        
        if (inventory[index].car_year < 2000) 
        {
            olderMade.push(inventory[index].car_make);
        }
    }
    return olderMade;
}

module.exports = problem5;