function problem6(inventory) {
    let arrayFilter = [];

    if (Array.isArray(inventory) === false || inventory === undefined) {
        return [];
    }
    if (inventory.length === 0) {
        return [];
    } 
    for (let index = 0; index < inventory.length; index++) {

        if (inventory[index].car_make === "BMW" || inventory[index].car_make === "Audi") {
            arrayFilter.push(inventory[index]);
        }
    }
    return arrayFilter;
}

module.exports = problem6;